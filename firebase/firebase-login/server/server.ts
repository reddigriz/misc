/**Dependencies */
import express from "express";

/** Test */

/**Internal Modules */

import * as firebaseMiddleware from "./firebase_middleware";

const app = express();

/*Middlewere*/
app.use("/firebaselogin", (req, res, next) => {
  console.log(req, res, next);
  firebaseMiddleware.verifyUid(req, res);
});

/**Routes */

/** SERVER */

app
  .listen(3000, async () => {
    console.warn(
      `| Running express: http://127.0.0.1:${3000} in ${process.env.NODE_ENV}/`
    );
  })
  .setTimeout(300000);
// function getAuth(firebase_app: FirebaseApp) {
//   throw new Error("Function not implemented.");
// }
