import { Request, Response } from "express";
import * as firebase_admin from "firebase-admin";

const firebase_app = firebase_admin.initializeApp(
  JSON.parse(process.env.FIREBASE_SECRET!),
  "firebase_app"
);
/**
 * This module is used to validate firestore uids after a successful auth fron clients.
 *
 * 1. `Uid received`: an uid is sent by the client to the server at the /firestore/login endpoint, check ./auth.ts
 *
 * 2. `Uid verification`: The received uid is verified, if successful the user may go on otherwise an error is thrown
 *
 */

/**
 * Check token revocation
 * https://firebase.google.com/docs/auth/admin/manage-sessions#detect_id_token_revocation
 */

function firebase_verification(token: string) {
  //https://stackoverflow.com/questions/40752739/what-does-firebase-server-side-verifyidtoken-do-under-the-hood

  return firebase_admin
    .auth(firebase_app)
    .verifyIdToken(token)
    .then((decodedToken: any) => {
      console.log(decodedToken);
      return true;
    })
    .catch((e: Error) => {
      console.log(e);
      return false;
    });
}

export async function verifyUid(req: Request, res: Response) {
  var token_status = false;
  if (req.body.uid != undefined) {
    token_status = await firebase_verification(req.body.uid);
  } else {
    token_status = false;
  }
  //const token_status = req.body.uid === undefined ? await firebase_verification(req.body.uid) : false;
  console.log(token_status);
  res.send(token_status);
}
