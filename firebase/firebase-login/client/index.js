// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { signInWithEmailAndPassword, getAuth, getIdToken } from "firebase/auth";
import axios from "axios";
import * as firebaseConfig from './serviceAccountKey.json';
// Your web app's Firebase configuration


const app = initializeApp(firebaseConfig);

const app_auth = getAuth(app);

async function getUid(user) {
  getIdToken(user, true)
  .then((idToken) => {
    axios
      .post("http://127.0.0.1:3000/firebaselogin", {
        uid: idToken,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  })
  .catch(function (error) {
    return 0;
  });
}

async function getFirebaseUid(email, password) {
  console.log("Doing email sign ")
  return signInWithEmailAndPassword(app_auth, email, password).then(
    async (userCredential) => {
      console.log("Signed in")
      console.log(userCredential.user); // Signed in
      const user = userCredential.user;
      console.log("gettin token")
      await getUid(user)
    }
  );
}

async function signIn(email, password) {
  await getFirebaseUid(email, password);
  console.log("done")
}

signIn("example@email.com", "password");
